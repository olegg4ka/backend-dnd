<?php


use Core\Components\Database\Connection\ConnectionFactory;
use Core\Components\Database\Connection\ConnectionInterface;
use Psr\Container\ContainerInterface;

return [
    ConnectionInterface::class =>  DI\factory(function (ContainerInterface $container){
        return new ConnectionFactory($container->get('config')['DataBase']);
    }),
    'ConnectDatabase' => DI\factory(function (ConnectionInterface $connection): ConnectionFactory {
        return $connection;
    })
];
