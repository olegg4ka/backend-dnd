<?php
//
//use Core\Backend\Component\Auth\Users\UserInterface;
//use Core\Backend\Component\Logger\LoggerFactory;
//use Core\Backend\Component\Logger\LoggerFactoryInterface;
//use Monolog\Logger;
//use Psr\Container\ContainerInterface;
//use Psr\Log\LoggerInterface;
//
//return [
//    LoggerInterface::class => DI\factory(function (ContainerInterface $container) {
//        $config = $container->get('config')['logger'];
//        return new Logger($config['nameLogger']);
//    }),
//    LoggerFactoryInterface::class => DI\factory(function (LoggerFactory $loggerFactory) {
//        return $loggerFactory;
//    }),
//    'LoggerDataBase' => DI\factory(function (ContainerInterface $container, LoggerFactoryInterface $loggerFactory) {
//
//        $userID = $container->get(UserInterface::class)->getUserID();
//        if ($userID) {
//            return $loggerFactory
//                ->setPath('users/' . date('Y') . '/' . date('m') . '/' . date('d') . '/' . $userID . '/')
//                ->createLogger('db1');
//        }
//
//        if ($userID === null) {
//            return $loggerFactory
//                ->setPath('users/' . date('Y') . '/' . date('m') . '/' . date('d') . '/null/')
//                ->createLogger('db1');
//        }
//
//        return $loggerFactory
//            ->createLogger('db1');
//    })
//];
