<?php


namespace Core\Components\Database\Connection;

use Exception;
use PDO;

/**
 *
 */
class ConnectionFactory implements ConnectionInterface
{
	private $config;
	private $connection = [];

	/**
	 * @param array $config
	 */
	public function __construct(array $config)
	{
		$this->config = $config;
	}

	/**
	 * @param string $nameConnection
	 * @return PDO
	 */
	private function createConnection(string $nameConnection): PDO
	{
		$config = $this->config[$nameConnection];
		try {
			$connection = new Pdo($config['dsn'], $config['username'], $config['password']);
			$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (Exception $e) {
			dump($e);
		}
		return $connection;
	}

	/**
	 * @param string $nameConnection
	 * @return mixed|PDO
	 * @throws Exception
	 */
	public function getConnection(string $nameConnection = 'dnd')
	{
		if (!isset($this->config[$nameConnection])) {
			throw new Exception('Підключення з таким ім\'ям відсутнє (nameConnection=' . $nameConnection . ')');
		}

		if (!isset($this->connection[$nameConnection])) {
			$this->connection[$nameConnection] = $this->createConnection($nameConnection);
		}

		return $this->connection[$nameConnection];
	}
}
