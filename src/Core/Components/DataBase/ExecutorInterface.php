<?php

namespace Core\Components\Database;

use Core\Components\Database\Connection\ConnectionInterface;

/**
 *
 */
interface ExecutorInterface
{
	/**
	 * @param ConnectionInterface $connection
	 */
    public function __construct(ConnectionInterface $connection);

    /**
     * @param string $query
     * @param array $values
     * @param $fetch
     * @param string $nameDatabase
     * @return mixed
     */
    public function execute(string $query, array $values, $fetch, string $nameDatabase);
}
