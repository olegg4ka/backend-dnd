<?php

namespace Core\Components\Database;

use Core\Components\Database\Connection\ConnectionInterface;
use Exception;
use PDO;

/**
 *
 */
class Executor
{

	private $connection;

	/**
	 * ProceduresExecutor constructor.
	 * @param ConnectionInterface $connection
	 */
	public function __construct(ConnectionInterface $connection)
	{
		$this->connection = $connection;
	}

	/**
	 * @param string $query
	 * @param array $values
	 * @param $fetch
	 * @param string $nameDatabase
	 * @return array|false
	 * @throws Exception
	 */
	public function execute(string $query, array $values, $fetch = PDO::FETCH_ASSOC, string $nameDatabase = 'dnd')
	{
		$connection = $this->connection->getConnection($nameDatabase);
		$db = $connection->prepare($query);
		$db->execute($values);
		return $db->fetchAll($fetch);
	}
}
