<?php

namespace Core\Components\Auth\User;

/**
 * Class User
 * @package Core\Components\Auth\User
 */
class User implements UserInterface
{
	/**
	 * @var mixed|null
	 */
    private $userID;

	/**
	 * @var mixed|null
	 */
    private $userName;

	/**
	 * User constructor.
	 */
    public function __construct()
    {
        if(isset($_SESSION['id'])){
            $this->userID = $_SESSION['id'];
        }else{
            $this->userID = null;
        }
	    if(isset($_SESSION['name'])){
		    $this->userName = $_SESSION['name'];
	    }else{
		    $this->userName = null;
	    }
    }

    /**
     * @return mixed
     */
    public function getUserID()
    {
        return $this->userID;
    }

	/**
	 * @param $userID
	 */
    public function setUserID($userID): void
    {
        $_SESSION['id'] = $userID;
        $this->userID = $userID;
    }

	/**
	 * @return string
	 */
	public function getUserName():string
	{
		return $this->userName;
	}
}
