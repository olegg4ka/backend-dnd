<?php

namespace Core\Components\Auth\User;

/**
 * Interface UserInterface
 * @package Core\Components\Auth\User
 */
interface UserInterface
{
	/**
	 * @return mixed
	 */
    public function getUserID();

	/**
	 * @param $userID
	 * @return mixed
	 */
    public function setUserID($userID);

	/**
	 * @return mixed
	 */
    public function getUserName();
}
