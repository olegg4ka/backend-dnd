<?php

namespace Core\Components\Redis;

use Redis;

/**
 *
 */
class RedisConnection
{
    private static $connection;

	/**
	 * @param int $dbIndex
	 * @return Redis
	 */
    public static function getConnection(int $dbIndex): Redis
    {
        if(self::$connection){
            self::$connection->select($dbIndex);
            return self::$connection;
        }else{
            self::$connection = (new RedisFactory())->createConnection();
            self::$connection->select($dbIndex);
            return self::$connection;
        }
    }

}