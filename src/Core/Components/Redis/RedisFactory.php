<?php

namespace Core\Components\Redis;

use Core\Components\Container\Container;
use Redis;

/**
 *
 */
class RedisFactory
{

	/**
	 * Створюємо підключення до бази
	 * @return Redis
	 */
    public function createConnection(): Redis
    {
        $config = Container::get('config')['redis'];

        $connection = new Redis();
        $connection->connect(
            $config['host'],
            $config['port']
        );
        return $connection;
    }
}
