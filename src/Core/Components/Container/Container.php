<?php

namespace Core\Components\Container;

/**
 * Class Container
 * @package Core\Components\Container
 */
class Container
{
    static $instance;

	/**
	 * @param Object $container
	 */
    public static function init(Object $container)
    {
        static::$instance = $container;
    }

	/**
	 * @param $name
	 * @return mixed
	 */
    public static function get($name)
    {
        return static::$instance->get($name);
    }

    /**
     * @param $name
     * @return mixed
     */
    public static function set($name)
    {
        return static::$instance->set($name);
    }
}
