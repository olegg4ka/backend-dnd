<?php

namespace App\Model\HazardType;


use Core\Components\Container\Container;
use Core\Components\Database\Executor;
use Core\Components\Redis\Storage\MenuStore;

/**
 * HazardType
 */
class HazardType
{
	/**
	 * @var array
	 */
	private $hazardType;

	/**
	 *
	 */
	public function __construct()
	{
		$this->hazardType = self::getHazardTypeData();
	}

	/**
	 * @return array
	 */
	private function getHazardTypeData(): array
	{
		$executor = Container::get(Executor::class);
		return $executor->execute("SELECT * FROM dnd.hazardTypes", [], 2, 'dnd');
	}

	/**
	 * @return array
	 */
	public function getHazardType(): array
	{
		return $this->hazardType;
	}

}