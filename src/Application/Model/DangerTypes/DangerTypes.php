<?php

namespace App\Model\HazardType;


use Core\Components\Container\Container;
use Core\Components\Database\Executor;
use Core\Components\Redis\Storage\MenuStore;

/**
 * HazardType
 */
class DangerTypes
{
	/**
	 * @var array
	 */
	private $dangerTypes;

	/**
	 *
	 */
	public function __construct()
	{
		$this->dangerTypes = self::getDangerTypesData();
	}

	/**
	 * @return array
	 */
	private function getDangerTypesData(): array
	{
		$executor = Container::get(Executor::class);
		return $executor->execute("SELECT * FROM dnd.dangerTypes", [], 2, 'dnd');
	}

	/**
	 * @return array
	 */
	public function getDangerTypes(): array
	{
		return $this->dangerTypes;
	}

}