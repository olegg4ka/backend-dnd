<?php

namespace App\Model\Menu;


use Core\Components\Container\Container;
use Core\Components\Database\Executor;
use Core\Components\Redis\Storage\MenuStore;

/**
 * Menu
 */
class Menu
{
	/**
	 * @var array
	 */
	private $menu;

	/**
	 *
	 */
	public function __construct()
	{
		$this->menu = self::getMenuData();
	}

	/**
	 * @return array
	 */
	private function getMenuData(): array
	{
		$executor = Container::get(Executor::class);
		$result = $executor->execute("SELECT * FROM dnd.menu", [], 2, 'dnd');
		(new MenuStore())->setMenuByID($result, 1);
		return $result;
	}

	/**
	 * @return array
	 */
	public function getMenu(): array
	{
		return $this->menu;
	}

}