<?php
/**
 * DB
 */
return [
	'config' => [
		'DataBase' => [
			'dnd' => [
				'host' =>'185.219.82.166',
				'port' =>'3310',
				'nameDatabase' =>'dnd',
				'username' =>'dnd',
				'password' =>'oB2yB1tB',
				'dsn' =>'mysql:dbname=dnd;host=185.219.82.166;port=3310;charset=utf8',
				'charset' =>'utf8',
				'attributes' => [
					PDO::ATTR_CASE => PDO::CASE_LOWER,
					PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
					PDO::ATTR_ORACLE_NULLS => PDO::NULL_TO_STRING,
					PDO::ATTR_STRINGIFY_FETCHES => false,
					PDO::SQLSRV_ATTR_FETCHES_NUMERIC_TYPE => true,
				]
			]
		],
	]
];