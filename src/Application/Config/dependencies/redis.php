<?php
return [
	'config' => [
		'redis' => [
			'host' => '127.0.0.1',
			'port' => '6379',
		]
	]
];