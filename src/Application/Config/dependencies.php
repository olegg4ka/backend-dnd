<?php
use Laminas\ConfigAggregator\ConfigAggregator;
use Laminas\ConfigAggregator\PhpFileProvider;

$aggregator = new ConfigAggregator([
    new PhpFileProvider('../src/Application/Config/dependencies/db.php'),
    new PhpFileProvider('../src/Application/Config/dependencies/redis.php'),
	new PhpFileProvider('../src/Core/Dependencies/database.php'),
	new PhpFileProvider('../src/Core/Dependencies/user.php'),
]);

return $aggregator->getMergedConfig();