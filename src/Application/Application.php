<?php


namespace App;


use App\Model\Menu\Menu;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpFoundation\Session\Session;


class Application
{
	/**
	 * Application constructor.
	 * @param SymfonyRequest|null $request
	 */
	public function __construct(SymfonyRequest $request = null)
	{
		$this->startSession();
	}

	/**
	 * @param null $storage
	 * @return Session
	 */
	public function startSession($storage = null): Session
	{
		$session = new Session($storage);
		$session->start();
		return $session;
	}

	/**
	 *
	 */
	public static function startApp()
	{
		$startData = null;
		if(isset($_SESSION['id']) && $_SESSION['id'] != 1){
			$startData = ['menu'=>(new Menu())->getMenu()];
		}else{
			$startData = ['menu'=>(new Menu())->getMenu()];
			$_SESSION['id'] = 1;
		}

	}
}